package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.config.SecurityConfig;
import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyRequest;
import com.alevel.java.nix.universitychecker.service.SpecialtyCategoryOperations;
import com.alevel.java.nix.universitychecker.service.SpecialtyOperations;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureWebMvc
@ExtendWith(SpringExtension.class)
@ActiveProfiles(value = "test")
@Import(SecurityConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpecialtyControllerTest {

    @Autowired
    private WebApplicationContext context;


    private MockMvc mockMvc;

    @MockBean
    private SpecialtyCategoryOperations specialtyCategoryOperations;

    @MockBean
    private SpecialtyCategory specialtyCategory;

    @BeforeEach
    void setUp() {

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

    }


    @MockBean
    private SpecialtyOperations specialtyOperations;

    private static final String SPECIALTY_NAME = "specialty";
    private static final int TEST_ID = 101;
    private static final String TEST_CATEGORY_NAME = "test";


    @Test
    void shouldGetAllSpecialtiesSuccessfully() throws Exception {
        Specialty specialty = new Specialty(1, SPECIALTY_NAME, specialtyCategory);
        Specialty specialty2 = new Specialty(2, SPECIALTY_NAME, specialtyCategory);
        when(specialtyOperations.getAll()).thenReturn(List.of(specialty, specialty2));
        when(specialtyCategory.getName()).thenReturn(TEST_CATEGORY_NAME);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/specialties"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].specialtyCategoryName", is(TEST_CATEGORY_NAME)));

    }

    @Test
    void testCreateSpecialty() throws Exception {
        Specialty specialty = new Specialty(1, SPECIALTY_NAME, specialtyCategory);
        SaveSpecialtyRequest saveSpecialtyRequest = new SaveSpecialtyRequest();
        saveSpecialtyRequest.setId(specialty.getId());
        saveSpecialtyRequest.setName(specialty.getName());
        saveSpecialtyRequest.setSpecialtyCategoryName(TEST_CATEGORY_NAME);
        when(specialtyOperations.create(any())).thenReturn(specialty);
        when(specialtyCategory.getName()).thenReturn(TEST_CATEGORY_NAME);
        mockMvc.perform(post("/specialties")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(saveSpecialtyRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is(SPECIALTY_NAME)))
                .andExpect(jsonPath("$.specialtyCategoryName", is(TEST_CATEGORY_NAME)));

    }

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
