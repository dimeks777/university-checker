package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyRequest;
import com.alevel.java.nix.universitychecker.entity.response.SpecialtyResponse;
import com.alevel.java.nix.universitychecker.service.SpecialtyCategoryOperations;
import com.alevel.java.nix.universitychecker.service.SpecialtyOperations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpecialtyOperationsTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private SpecialtyCategoryOperations specialtyCategoryOperations;

    @Autowired
    private SpecialtyOperations specialtyOperations;


    @BeforeEach
    void setUp() {

        var specialtyCategoryRequest = new SaveSpecialtyCategoryRequest();
        specialtyCategoryRequest.setName("test1");
        specialtyCategoryOperations.create(specialtyCategoryRequest);
        specialtyCategoryRequest = new SaveSpecialtyCategoryRequest();
        specialtyCategoryRequest.setName("test2");
        specialtyCategoryOperations.create(specialtyCategoryRequest);

    }


    @AfterEach
    void end() {
        specialtyOperations.deleteAll();
        specialtyCategoryOperations.deleteAll();
    }


    @Test
    void testContextLoads() {
        assertNotNull(rest);
        assertNotEquals(0, port);
    }


    @Test
    void testCreateSpecialty() {

        Integer id = 101;
        String name = "specialty";
        String category = "test1";
        ResponseEntity<SpecialtyResponse> responseResponseEntity = createSpecialty(id, name, category);
        assertEquals(HttpStatus.CREATED, responseResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseResponseEntity.getHeaders().getContentType());
        SpecialtyResponse responseBody = responseResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(id, responseBody.getId());
        assertEquals(name, responseBody.getName());
        assertEquals(category, responseBody.getSpecialtyCategoryName());

    }


    @Test
    void testGetSpecialtyById() {

        Integer id = 111;
        String name = "spec";
        String category = "test2";
        var specialtyResponse = createSpecialty(id, name, category).getBody();
        assertNotNull(specialtyResponse);
        Integer idCreated = specialtyResponse.getId();
        var specialtyUrl = "http://localhost:" + port + "/specialties/" + idCreated;
        ResponseEntity<SpecialtyResponse> specialtyResponseEntity = rest.getForEntity(specialtyUrl, SpecialtyResponse.class);
        assertEquals(HttpStatus.OK, specialtyResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, specialtyResponseEntity.getHeaders().getContentType());
        SpecialtyResponse responseBody = specialtyResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(name, responseBody.getName());
        assertEquals(category, responseBody.getSpecialtyCategoryName());
        assertEquals(id, responseBody.getId());

    }


    @Test
    void testGetNonExistingSpecialty() {

        final int nonExistingId = 999;
        var specialtyUrl = "http://localhost:" + port + "/specialties/" + nonExistingId;
        ResponseEntity<SpecialtyResponse> specialtyResponseEntity = rest.getForEntity(specialtyUrl, SpecialtyResponse.class);
        assertEquals(HttpStatus.NOT_FOUND, specialtyResponseEntity.getStatusCode());

    }


    @Test
    void testUpdateSpecialty() {

        Integer id = 999;
        String name = "compucter";
        String category = "test1";
        var specialtyResponse = createSpecialty(id, name, category).getBody();
        assertNotNull(specialtyResponse);
        Integer idCreated = specialtyResponse.getId();
        var specialtyUrl = "http://localhost:" + port + "/specialties/" + idCreated;
        Integer updatedId = 111;
        String updatedName = "updated";
        String updatedCategory = "test2";
        var request = new SaveSpecialtyRequest(updatedId, updatedName, updatedCategory);
        rest.put(specialtyUrl, request);
        ResponseEntity<SpecialtyResponse> specialtyResponseEntity = rest.getForEntity(specialtyUrl, SpecialtyResponse.class);
        assertEquals(HttpStatus.OK, specialtyResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, specialtyResponseEntity.getHeaders().getContentType());
        SpecialtyResponse responseBody = specialtyResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(idCreated, responseBody.getId());
        assertEquals(updatedName, responseBody.getName());
        assertEquals(updatedCategory, responseBody.getSpecialtyCategoryName());

    }


    @Test
    void testDeleteSpecialty() {

        Integer id = 777;
        String name = "compucter";
        String category = "test1";
        var specialtyResponse = createSpecialty(id, name, category).getBody();
        assertNotNull(specialtyResponse);
        Integer idCreated = specialtyResponse.getId();
        var specialtyUrl = "http://localhost:" + port + "/specialties/" + idCreated;
        var specialtyUri = URI.create(specialtyUrl);
        ResponseEntity<SpecialtyResponse> specialtyResponseEntity = rest
                .exchange(RequestEntity.delete(specialtyUri).build(), SpecialtyResponse.class);
        assertEquals(HttpStatus.OK, specialtyResponseEntity.getStatusCode());
        assertNull(specialtyResponseEntity.getHeaders().getContentType());
        SpecialtyResponse responseBody = specialtyResponseEntity.getBody();
        assertNull(responseBody);
        assertEquals(HttpStatus.NOT_FOUND, rest
                .getForEntity(specialtyUrl, SpecialtyResponse.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(specialtyUri).build(), SpecialtyResponse.class)
                .getStatusCode());
    }


    private ResponseEntity<SpecialtyResponse> createSpecialty(Integer id, String name, String category) {

        String url = "http://localhost:" + port + "/specialties";
        var requestBody = new SaveSpecialtyRequest(id, name, category);
        return rest.postForEntity(url, requestBody, SpecialtyResponse.class);

    }
}
