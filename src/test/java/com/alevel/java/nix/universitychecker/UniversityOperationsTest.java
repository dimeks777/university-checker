package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.Location;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyRequest;
import com.alevel.java.nix.universitychecker.entity.request.SaveUniversityRequest;
import com.alevel.java.nix.universitychecker.entity.response.UniversityResponse;
import com.alevel.java.nix.universitychecker.service.SpecialtyCategoryOperations;
import com.alevel.java.nix.universitychecker.service.SpecialtyOperations;
import com.alevel.java.nix.universitychecker.service.UniversityOperations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UniversityOperationsTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private SpecialtyCategoryOperations specialtyCategoryOperations;

    @Autowired
    private SpecialtyOperations specialtyOperations;

    @Autowired
    private UniversityOperations universityOperations;

    @BeforeEach
    void setUp() {

        var specialtyCategoryRequest = new SaveSpecialtyCategoryRequest();
        specialtyCategoryRequest.setName("test1");
        specialtyCategoryOperations.create(specialtyCategoryRequest);
        specialtyCategoryRequest = new SaveSpecialtyCategoryRequest();
        specialtyCategoryRequest.setName("test2");
        specialtyCategoryOperations.create(specialtyCategoryRequest);
        var specialtyRequest = new SaveSpecialtyRequest();
        specialtyRequest.setId(111);
        specialtyRequest.setName("spec1");
        specialtyRequest.setSpecialtyCategoryName("test1");
        specialtyOperations.create(specialtyRequest);
        specialtyRequest.setId(222);
        specialtyRequest.setName("spec2");
        specialtyRequest.setSpecialtyCategoryName("test2");
        specialtyOperations.create(specialtyRequest);

    }


    @AfterEach
    void end() {
        universityOperations.deleteAll();
        specialtyOperations.deleteAll();
        specialtyCategoryOperations.deleteAll();
    }


    @Test
    void testContextLoads() {
        assertNotNull(rest);
        assertNotEquals(0, port);
    }


    @Test
    void testCreateUniversity() {

        String name = "university";
        Set<Integer> set = Set.of(111, 222);
        Double points = 67.5;
        Location location = new Location();
        location.setCountry("Kazakhstan");
        location.setCity("Astana");
        ResponseEntity<UniversityResponse> responseResponseEntity = createUniversity(name, set, points, location);
        assertEquals(HttpStatus.CREATED, responseResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseResponseEntity.getHeaders().getContentType());
        UniversityResponse responseBody = responseResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(name, responseBody.getName());
        assertEquals(points, responseBody.getOfficialPoints());
        assertEquals(location.toString(), responseBody.getLocation());

    }


    @Test
    void testGetUniversityById() {

        String name = "university";
        Set<Integer> set = Set.of(111, 222);
        Double points = 67.5;
        Location location = new Location();
        location.setCountry("Kazakhstan");
        location.setCity("Astana");
        var universityResponse = createUniversity(name, set, points, location).getBody();
        assertNotNull(universityResponse);
        Integer idCreated = universityResponse.getId();
        var universityUrl = "http://localhost:" + port + "/universities/" + idCreated;
        ResponseEntity<UniversityResponse> responseEntity = rest.getForEntity(universityUrl, UniversityResponse.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseEntity.getHeaders().getContentType());
        UniversityResponse responseBody = responseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(name, responseBody.getName());
        assertEquals(points, responseBody.getOfficialPoints());
        assertEquals(location.toString(), responseBody.getLocation());

    }


    @Test
    void testGetNonExistingUniversity() {

        final int nonExistingId = 999;
        var universityUrl = "http://localhost:" + port + "/universities/" + nonExistingId;
        ResponseEntity<UniversityResponse> responseEntity = rest.getForEntity(universityUrl, UniversityResponse.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

    }


    @Test
    void testUpdateUniversity() {

        String name = "university";
        Set<Integer> set = Set.of(111, 222);
        Double points = 67.5;
        Location location = new Location();
        location.setCountry("Kazakhstan");
        location.setCity("Astana");
        var universityResponse = createUniversity(name, set, points, location).getBody();
        assertNotNull(universityResponse);
        Integer idCreated = universityResponse.getId();
        var universityUrl = "http://localhost:" + port + "/universities/" + idCreated;
        String updatedName = "college";
        Set<Integer> specs = Set.of(222);
        Double updatedPoints = 43.2;
        Location updatedLocation = new Location();
        updatedLocation.setCountry("Russia");
        updatedLocation.setCity("Moscow");
        var request = new SaveUniversityRequest();
        request.setName(updatedName);
        request.setSpecialtyIds(specs);
        request.setOfficialPoints(updatedPoints);
        request.setLocation(updatedLocation);
        rest.put(universityUrl, request);
        ResponseEntity<UniversityResponse> universityResponseResponseEntity = rest.getForEntity(universityUrl, UniversityResponse.class);
        assertEquals(HttpStatus.OK, universityResponseResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, universityResponseResponseEntity.getHeaders().getContentType());
        UniversityResponse responseBody = universityResponseResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(idCreated, responseBody.getId());
        assertEquals(updatedName, responseBody.getName());
        assertEquals(updatedPoints, responseBody.getOfficialPoints());
        assertEquals(updatedLocation.toString(), responseBody.getLocation());
    }


    @Test
    void testDeleteUniversity() {

        String name = "university";
        Set<Integer> set = Set.of(111, 222);
        Double points = 67.5;
        Location location = new Location();
        location.setCountry("Kazakhstan");
        location.setCity("Astana");
        var universityResponse = createUniversity(name, set, points, location).getBody();

        assertNotNull(universityResponse);

        Integer idCreated = universityResponse.getId();

        var universityUrl = "http://localhost:" + port + "/universities/" + idCreated;

        var universityUri = URI.create(universityUrl);

        ResponseEntity<UniversityResponse> universityResponseResponseEntity = rest

                .exchange(RequestEntity.delete(universityUri).build(), UniversityResponse.class);

        assertEquals(HttpStatus.OK, universityResponseResponseEntity.getStatusCode());
        assertNull(universityResponseResponseEntity.getHeaders().getContentType());
        UniversityResponse responseBody = universityResponseResponseEntity.getBody();
        assertNull(responseBody);
        assertEquals(HttpStatus.NOT_FOUND, rest
                .getForEntity(universityUrl, UniversityResponse.class).getStatusCode());

        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(universityUri).build(), UniversityResponse.class)
                .getStatusCode());

    }


    private ResponseEntity<UniversityResponse> createUniversity(String name, Set<Integer> specialties, Double points, Location location) {

        String url = "http://localhost:" + port + "/universities";
        var requestBody = new SaveUniversityRequest();
        requestBody.setName(name);
        requestBody.setSpecialtyIds(specialties);
        requestBody.setOfficialPoints(points);
        requestBody.setLocation(location);
        return rest.postForEntity(url, requestBody, UniversityResponse.class);

    }
}
