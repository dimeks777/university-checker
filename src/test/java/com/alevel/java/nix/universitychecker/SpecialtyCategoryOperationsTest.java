package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;
import com.alevel.java.nix.universitychecker.entity.response.SpecialtyCategoryResponse;
import com.alevel.java.nix.universitychecker.service.SpecialtyCategoryOperations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpecialtyCategoryOperationsTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private SpecialtyCategoryOperations specialtyCategoryOperations;

    @AfterEach
    void end() {
        specialtyCategoryOperations.deleteAll();
    }

    @Test
    void testContextLoads() {
        assertNotNull(rest);
        assertNotEquals(0, port);
    }

    @Test
    void createCategory() {
        var name = "test";
        ResponseEntity<SpecialtyCategoryResponse> specialtyCategoryResponseEntity = createSpecialtyCategory(name);
        assertEquals(HttpStatus.CREATED, specialtyCategoryResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, specialtyCategoryResponseEntity.getHeaders().getContentType());
        SpecialtyCategoryResponse responseBody = specialtyCategoryResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(name, responseBody.getName());
        assertNotNull(responseBody.getId());
    }


    @Test
    void testGetCategoryById() {

        String name = "test";
        var specialtyCategory = createSpecialtyCategory(name).getBody();
        assertNotNull(specialtyCategory);
        Integer category = specialtyCategory.getId();
        var specialtyCategoryUrl = "http://localhost:" + port + "/specialty_categories/" + category;
        ResponseEntity<SpecialtyCategoryResponse> categoryResponseEntity = rest.getForEntity(specialtyCategoryUrl, SpecialtyCategoryResponse.class);
        assertEquals(HttpStatus.OK, categoryResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, categoryResponseEntity.getHeaders().getContentType());
        SpecialtyCategoryResponse responseBody = categoryResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(name, responseBody.getName());
        assertNotNull(responseBody.getId());

    }

    @Test
    void testGetNonExistingCategory() {

        final int nonExistingId = 999;
        var categoryUrl = "http://localhost:" + port + "/specialty_categories/" + nonExistingId;
        ResponseEntity<SpecialtyCategoryResponse> categoryResponseEntity = rest.getForEntity(categoryUrl, SpecialtyCategoryResponse.class);
        assertEquals(HttpStatus.NOT_FOUND, categoryResponseEntity.getStatusCode());

    }


    @Test
    void testUpdateCategory() {

        String name = "before";
        var specialtyCategory = createSpecialtyCategory(name).getBody();
        assertNotNull(specialtyCategory);
        String id = specialtyCategory.getName();
        var categoryUrl = "http://localhost:" + port + "/specialty_categories/" + id;
        String updatedName = "after";
        var request = new SaveSpecialtyCategoryRequest();
        request.setName(updatedName);
        rest.put(categoryUrl, request);
        var categoryUrl2 = "http://localhost:" + port + "/specialty_categories/" + specialtyCategory.getId();
        ResponseEntity<SpecialtyCategoryResponse> categoryResponseEntity = rest.getForEntity(categoryUrl2, SpecialtyCategoryResponse.class);
        assertEquals(HttpStatus.OK, categoryResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, categoryResponseEntity.getHeaders().getContentType());
        SpecialtyCategoryResponse responseBody = categoryResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(updatedName, responseBody.getName());
        assertEquals(specialtyCategory.getId(), responseBody.getId());

    }


    @Test
    void testDeleteCategory() {
        String name = "testrest";
        var category = createSpecialtyCategory(name).getBody();
        assertNotNull(category);
        var categoryUrl = "http://localhost:" + port + "/specialty_categories/" + name;
        var categoryUri = URI.create(categoryUrl);
        ResponseEntity<SpecialtyCategoryResponse> categoryResponseEntity = rest
                .exchange(RequestEntity.delete(categoryUri).build(), SpecialtyCategoryResponse.class);
        assertEquals(HttpStatus.OK, categoryResponseEntity.getStatusCode());
        assertNull(categoryResponseEntity.getHeaders().getContentType());
        SpecialtyCategoryResponse responseBody = categoryResponseEntity.getBody();
        assertNull(responseBody);
        categoryUrl = "http://localhost:" + port + "/specialty_categories/" + category.getId();
        categoryUri = URI.create(categoryUrl);
        assertEquals(HttpStatus.NOT_FOUND, rest
                .getForEntity(categoryUrl, SpecialtyCategoryResponse.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(categoryUri).build(), SpecialtyCategoryResponse.class)
                .getStatusCode());

    }

    private ResponseEntity<SpecialtyCategoryResponse> createSpecialtyCategory(String name) {
        var url = "http://localhost:" + port + "/specialty_categories";
        var requestBody = new SaveSpecialtyCategoryRequest();
        requestBody.setName(name);
        return rest.postForEntity(url, requestBody, SpecialtyCategoryResponse.class);
    }


}
