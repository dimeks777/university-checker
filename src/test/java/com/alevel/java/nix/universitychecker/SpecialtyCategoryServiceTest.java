package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;
import com.alevel.java.nix.universitychecker.exception.SpecialtyCategoryNotFoundException;
import com.alevel.java.nix.universitychecker.repository.SpecialtyCategoryRepository;
import com.alevel.java.nix.universitychecker.service.SpecialtyCategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SpecialtyCategoryServiceTest {

    @Mock
    private SpecialtyCategoryRepository specialtyCategoryRepository;

    private SpecialtyCategoryService specialtyCategoryService;

    @Mock
    private SpecialtyCategory specialtyCategory;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        specialtyCategoryService = Mockito.spy(new SpecialtyCategoryService(specialtyCategoryRepository));
    }

    @Test
    void shouldGetSpecialtyCategoryByIdSuccessfully() {
        doReturn(specialtyCategory).when(specialtyCategoryService).getById(5);

        SpecialtyCategory retrieved = specialtyCategoryService.getById(5);

        verify(specialtyCategoryService, times(1)).getById(5);
    }

    @Test
    void shouldVerifyGetSpecialtyCategoryByIdIsNotCallSave() {
        doReturn(specialtyCategory).when(specialtyCategoryService).getById(5);

        SpecialtyCategory retrieved = specialtyCategoryService.getById(5);

        verify(specialtyCategoryRepository, never()).save(specialtyCategory);
    }

    @Test
    void shouldGetByIdThrowNullPointerException() {
        assertThrows(SpecialtyCategoryNotFoundException.class, () -> {
            SpecialtyCategory retrieved = specialtyCategoryService.getById(5);
            assertEquals(retrieved, specialtyCategory);
        });

    }

    @Test
    void shouldCreateSpecialtyCategorySuccessfully(){
        final SaveSpecialtyCategoryRequest request = new SaveSpecialtyCategoryRequest("test");
        when(specialtyCategoryRepository.save(any(SpecialtyCategory.class))).thenReturn(new SpecialtyCategory(request.getName()));
        SpecialtyCategory savedSpecialtyCategory = specialtyCategoryService.create(request);
        assertThat(savedSpecialtyCategory).isNotNull();
        verify(specialtyCategoryRepository).save(any(SpecialtyCategory.class));
    }

}
