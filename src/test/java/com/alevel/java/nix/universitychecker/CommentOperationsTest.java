package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.Location;
import com.alevel.java.nix.universitychecker.entity.Role;
import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.request.*;
import com.alevel.java.nix.universitychecker.entity.response.CommentResponse;
import com.alevel.java.nix.universitychecker.repository.RoleRepository;
import com.alevel.java.nix.universitychecker.service.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentOperationsTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private CommentOperations commentOperations;

    @Autowired
    private SpecialtyCategoryOperations specialtyCategoryOperations;

    @Autowired
    private SpecialtyOperations specialtyOperations;

    @Autowired
    private UniversityOperations universityOperations;

    @Autowired
    private UserOperations userOperations;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final int TEST_UNIVERSITY_ID = 1;
    private static final int TEST_USER_ID = 1;
    private static final double TEST_POINTS_VALUE = 35.0;
    private static final String TEST_TEXT_VALUE = "test";

    @BeforeEach
    void setUp() {
        roleRepository.save(new Role(UserRoles.USER));
        userOperations.create(new SaveRegularUserRequest("user", "123").toSaveRequest());
        specialtyCategoryOperations.create(new SaveSpecialtyCategoryRequest("test1"));
        specialtyCategoryOperations.create(new SaveSpecialtyCategoryRequest("test2"));
        specialtyOperations.create(new SaveSpecialtyRequest(111, "spec1", "test1"));
        specialtyOperations.create(new SaveSpecialtyRequest(222, "spec2", "test2"));
        universityOperations.create(new SaveUniversityRequest("university1", Set.of(111, 222), 50.0,
                new Location("country1", "city1")));
    }


    @AfterEach
    void end() {
        commentOperations.deleteAll();
        universityOperations.deleteAll();
        specialtyOperations.deleteAll();
        specialtyCategoryOperations.deleteAll();
        userOperations.deleteAll();
        roleRepository.deleteAll();
    }

    @Test
    void testContextLoads() {
        assertNotNull(rest);
        assertNotEquals(0, port);
    }

    @Test
    void createComment() {
        ResponseEntity<CommentResponse> commentResponseEntity = createCommentEntity();
        assertAll("Create comment response status test",
                () -> assertEquals(HttpStatus.CREATED, commentResponseEntity.getStatusCode()),
                () -> assertEquals(MediaType.APPLICATION_JSON, commentResponseEntity.getHeaders().getContentType())
        );
        CommentResponse responseBody = commentResponseEntity.getBody();
        assertNotNull(responseBody);
        assertAll("Create comment response test",
                () -> assertEquals(TEST_UNIVERSITY_ID, responseBody.getUniversityId()),
                () -> assertEquals(TEST_USER_ID, responseBody.getUserId()),
                () -> assertEquals(TEST_POINTS_VALUE, responseBody.getPoints()),
                () -> assertEquals(TEST_TEXT_VALUE, responseBody.getText()),
                () -> assertNotNull(responseBody.getId())
        );
    }


    @Test
    void testGetCommentById() {
        var comment = createCommentEntity().getBody();
        assertNotNull(comment);
        var id = comment.getId();
        var commentUrl = "http://localhost:" + port + "/comments/" + id;
        ResponseEntity<CommentResponse> commentResponseEntity = rest.getForEntity(commentUrl, CommentResponse.class);

        assertAll("Get comment by id response status test",
                () -> assertEquals(HttpStatus.OK, commentResponseEntity.getStatusCode()),
                () -> assertEquals(MediaType.APPLICATION_JSON, commentResponseEntity.getHeaders().getContentType())
        );

        CommentResponse responseBody = commentResponseEntity.getBody();
        assertNotNull(responseBody);
        assertAll("Get comment by id response test",
                () -> assertEquals(TEST_UNIVERSITY_ID, responseBody.getUniversityId()),
                () -> assertEquals(TEST_USER_ID, responseBody.getUserId()),
                () -> assertEquals(TEST_POINTS_VALUE, responseBody.getPoints()),
                () -> assertEquals(TEST_TEXT_VALUE, responseBody.getText()),
                () -> assertNotNull(responseBody.getId())
        );
    }

    @Test
    void testGetNonExistingCategory() throws Exception {

        final UUID nonExistingId = UUID.randomUUID();
        var commentUrl = "http://localhost:" + port + "/comments/" + nonExistingId;
        mockMvc.perform(MockMvcRequestBuilders.get(commentUrl)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


    @Test
    void testUpdateComment() {
        var comment = createCommentEntity().getBody();
        assertNotNull(comment);
        var id = comment.getId();
        var commentUrl = "http://localhost:" + port + "/comments/" + id;
        var updatedText = "after";
        var updatedPoints = 75.9;
        var request = new SaveCommentRequest();
        request.setText(updatedText);
        request.setPoints(updatedPoints);
        rest.put(commentUrl, request);
        ResponseEntity<CommentResponse> categoryResponseEntity = rest.getForEntity(commentUrl, CommentResponse.class);
        assertEquals(HttpStatus.OK, categoryResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, categoryResponseEntity.getHeaders().getContentType());
        CommentResponse responseBody = categoryResponseEntity.getBody();
        assertNotNull(responseBody);
        assertAll("After update comment response test",
                () -> assertEquals(updatedText, responseBody.getText()),
                () -> assertEquals(updatedPoints, responseBody.getPoints()),
                () -> assertEquals(TEST_UNIVERSITY_ID, responseBody.getUniversityId()),
                () -> assertEquals(TEST_USER_ID, responseBody.getUserId()),
                () -> assertNotNull(responseBody.getId())
        );

    }


    @Test
    void testDeleteComment() {
        var comment = createCommentEntity().getBody();
        assertNotNull(comment);
        var categoryUrl = "http://localhost:" + port + "/comments/" + comment.getId();
        var categoryUri = URI.create(categoryUrl);
        ResponseEntity<CommentResponse> commentResponseEntity = rest
                .exchange(RequestEntity.delete(categoryUri).build(), CommentResponse.class);
        assertEquals(HttpStatus.OK, commentResponseEntity.getStatusCode());
        assertNull(commentResponseEntity.getHeaders().getContentType());
        CommentResponse responseBody = commentResponseEntity.getBody();
        assertNull(responseBody);
        categoryUri = URI.create(categoryUrl);
        assertEquals(HttpStatus.NOT_FOUND, rest
                .getForEntity(categoryUrl, CommentResponse.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(categoryUri).build(), CommentResponse.class)
                .getStatusCode());

    }

    private ResponseEntity<CommentResponse> createCommentEntity() {
        var url = "http://localhost:" + port + "/comments";
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("userId", TEST_USER_ID)
                .queryParam("universityId", TEST_UNIVERSITY_ID);
        var requestBody = new SaveCommentRequest();
        requestBody.setPoints(TEST_POINTS_VALUE);
        requestBody.setText(TEST_TEXT_VALUE);
        String uri = uriBuilder.toUriString();
        return rest.postForEntity(uri, requestBody, CommentResponse.class);
    }


}
