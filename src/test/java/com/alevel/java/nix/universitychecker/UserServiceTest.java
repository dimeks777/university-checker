package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.Role;
import com.alevel.java.nix.universitychecker.entity.User;
import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.request.SaveRegularUserRequest;
import com.alevel.java.nix.universitychecker.exception.UsernameAlreadyExistsException;
import com.alevel.java.nix.universitychecker.repository.RoleRepository;
import com.alevel.java.nix.universitychecker.repository.UserRepository;
import com.alevel.java.nix.universitychecker.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userOperations;

    @BeforeEach
    void setUp() {
        final Role role = new Role(UserRoles.USER);
        lenient().when(roleRepository.findByName(UserRoles.USER))
                .thenReturn(Optional.of(role));
    }

    @Test
    void shouldGetUserByIdSuccessfully(){
        final User user = new User("user", "123");
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        User receivedUser = userOperations.getById(1);
        assertThat(receivedUser).isNotNull();
        assertEquals(user.getUsername(),receivedUser.getUsername());
        verify(userRepository,only()).findById(1);
    }

    @Test
    void shouldSaveRegularUserSuccessfully() {
        final SaveRegularUserRequest user = new SaveRegularUserRequest("user", "123");
        when(userRepository.existsByUsername(user.getUsername())).thenReturn(false);
        when(userRepository.save(any(User.class))).thenReturn(new User(user.getUsername(), user.getPassword()));
        User savedUser = userOperations.create(user.toSaveRequest());
        assertThat(savedUser).isNotNull();
        verify(passwordEncoder, times(1)).encode(anyString());
        verify(userRepository).save(any(User.class));
    }

    @Test
    void shouldThrowErrorWhenSaveRegularUser() {
        final User user = new User("user", "123");
        when(userRepository.existsByUsername(user.getUsername())).thenReturn(true);
        assertThrows(UsernameAlreadyExistsException.class, () ->
                userOperations.create(new SaveRegularUserRequest(user.getUsername(), user.getPassword()).toSaveRequest()));
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void shouldUpdateExistingUserSuccessfully() {
        final User user = new User(1, "user", "123");
        when(userRepository.save(user)).thenReturn(user);
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        userOperations.update(user.getId(),
                new SaveRegularUserRequest(user.getUsername(), user.getPassword()).toSaveRequest());
        verify(passwordEncoder, times(1)).encode(anyString());
        verify(userRepository).save(any(User.class));
    }


}
