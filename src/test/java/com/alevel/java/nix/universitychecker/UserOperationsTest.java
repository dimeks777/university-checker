package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.controller.restcontrollers.UserController;
import com.alevel.java.nix.universitychecker.entity.Role;
import com.alevel.java.nix.universitychecker.entity.User;
import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.request.SaveRegularUserRequest;
import com.alevel.java.nix.universitychecker.entity.response.UserResponse;
import com.alevel.java.nix.universitychecker.repository.RoleRepository;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserOperationsTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private UserOperations userOperations;

    @Autowired
    private RoleRepository roleRepository;

    @BeforeEach
    void setUp() {
        var role = new Role();
        role.setName(UserRoles.USER);
        roleRepository.save(role);
    }

    @AfterEach
    void end() {
        userOperations.deleteAll();
        roleRepository.deleteAll();
    }

    @Test
    void createUser() {
        var username = "user";
        var password = "password";
        ResponseEntity<UserResponse> userResponseEntity = postUser(username, password);
        assertEquals(HttpStatus.CREATED, userResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, userResponseEntity.getHeaders().getContentType());
        UserResponse responseBody = userResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(username, responseBody.getUsername());
        assertNotNull(responseBody.getId());
    }

    @Test
    void testGetUserById() {

        String username = "user";
        String password = "password";
        var user = postUser(username, password).getBody();
        assertNotNull(user);
        Integer id = user.getId();
        var userUrl = "http://localhost:" + port + "/users/" + id;
        ResponseEntity<UserResponse> userResponseEntity = rest.getForEntity(userUrl, UserResponse.class);
        assertEquals(HttpStatus.OK, userResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, userResponseEntity.getHeaders().getContentType());
        UserResponse responseBody = userResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(username, responseBody.getUsername());
        assertEquals(id, responseBody.getId());

    }


    @Test
    void testGetNonExistingUser() {

        final int nonExistingId = 999;
        var userUrl = "http://localhost:" + port + "/users/" + nonExistingId;
        ResponseEntity<UserResponse> userResponseEntity = rest.getForEntity(userUrl, UserResponse.class);
        assertEquals(HttpStatus.NOT_FOUND, userResponseEntity.getStatusCode());

    }


    @Test
    void testUpdateUser() {
        String username = "username";
        String password = "password";
        var user = postUser(username, password).getBody();
        assertNotNull(user);
        Integer id = user.getId();
        var userUrl = "http://localhost:" + port + "/users/" + id;
        String updatedUsername = "new";
        String updatedPassword = "new";
        var request = new SaveRegularUserRequest(updatedUsername, updatedPassword);
        rest.put(userUrl, request);
        ResponseEntity<UserResponse> userResponseEntity = rest.getForEntity(userUrl, UserResponse.class);
        assertEquals(HttpStatus.OK, userResponseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, userResponseEntity.getHeaders().getContentType());
        UserResponse responseBody = userResponseEntity.getBody();
        assertNotNull(responseBody);
        assertEquals(updatedUsername, responseBody.getUsername());
        assertEquals(id, responseBody.getId());

    }


    @Test
    void testDeleteUser() {

        String username = "username";
        String password = "password";
        var user = postUser(username, password).getBody();
        assertNotNull(user);
        Integer id = user.getId();
        var userUrl = "http://localhost:" + port + "/users/" + id;
        var userUri = URI.create(userUrl);
        ResponseEntity<UserResponse> userResponseEntity = rest
                .exchange(RequestEntity.delete(userUri).build(), UserResponse.class);
        assertEquals(HttpStatus.OK, userResponseEntity.getStatusCode());
        assertNull(userResponseEntity.getHeaders().getContentType());
        UserResponse responseBody = userResponseEntity.getBody();
        assertNull(responseBody);
        assertEquals(HttpStatus.NOT_FOUND, rest
                .getForEntity(userUrl, User.class).getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, rest
                .exchange(RequestEntity.delete(userUri).build(), User.class)
                .getStatusCode());
    }


    private ResponseEntity<UserResponse> postUser(String username, String password) {

        String url = "http://localhost:" + port + "/users";
        var requestBody = new SaveRegularUserRequest(username, password);
        return rest.postForEntity(url, requestBody, UserResponse.class);

    }


}
