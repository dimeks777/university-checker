package com.alevel.java.nix.universitychecker;

import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;
import com.alevel.java.nix.universitychecker.repository.SpecialtyCategoryRepository;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.junit5.DBUnitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@ExtendWith(DBUnitExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class CategoryDBTest {

    @Autowired
    private SpecialtyCategoryRepository repository;

    @Test
    @DataSet("categories.yml")
    void testFindAll() {
        List<SpecialtyCategory> categories = repository.findAll();
        Assertions.assertEquals(2, categories.size(), "Expected 2 categories in the database");
    }

    @Test
    @DataSet("categories.yml")
    void testFindByIdSuccess() {
        Optional<SpecialtyCategory> widget = repository.findById(1);
        Assertions.assertTrue(widget.isPresent(), "We should find a category with ID 1");

        SpecialtyCategory specialtyCategory = widget.get();
        Assertions.assertEquals(1, specialtyCategory.getId(), "The category ID should be 1");
        Assertions.assertEquals("Category 1", specialtyCategory.getName(), "Incorrect category name");
    }

    @Test
    @DataSet("categories.yml")
    void testFindByIdNotFound() {
        Optional<SpecialtyCategory> widget = repository.findById(3);
        Assertions.assertFalse(widget.isPresent(), "A category with ID 3 should not be found");
    }

    @Test
    @DataSet("categories.yml")
    @Commit
    public void testDeleteCategory() {
        repository.deleteById(1);
        List<SpecialtyCategory> categories = repository.findAll();
        Assertions.assertEquals(1, categories.size(), "Expected 0 categories in the database");
    }

    @Test
    @DataSet("categories.yml")
    public void testNotExistsByIdCategory() {
        var exist = repository.existsById(777);
        Assertions.assertFalse(exist, "Expected no category with id 777 in the database");
    }


}
