package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Comment;
import com.alevel.java.nix.universitychecker.entity.request.SaveCommentRequest;

import java.util.List;
import java.util.UUID;

public interface CommentOperations {

    Comment create(Integer userId, Integer universityId, SaveCommentRequest request);

    void update(UUID id, SaveCommentRequest request);

    Comment getById(UUID id);

    void deleteById(UUID id);

    void deleteAll();

    List<Comment> getAllByUniversityId(Integer universityId);

    List<Comment> getAllByUserId(Integer userId);

}
