package com.alevel.java.nix.universitychecker.entity.response;

import com.alevel.java.nix.universitychecker.entity.Comment;

import java.util.UUID;

public class CommentResponse {
    private UUID id;
    private String text;
    private Integer universityId;
    private Integer userId;
    private Double points;

    public CommentResponse(UUID id, String text, Integer universityId, Integer userId, Double points) {
        this.id = id;
        this.text = text;
        this.universityId = universityId;
        this.userId = userId;
        this.points = points;
    }

    public CommentResponse() {
    }

    public static CommentResponse fromComment(Comment comment) {
        return new CommentResponse(comment.getId(), comment.getText(), comment.getUniversity().getId(), comment.getUser().getId(), comment.getPoints());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Integer universityId) {
        this.universityId = universityId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }
}
