package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class CommentAlreadyExistsException extends UniversitiesCheckException {

    public CommentAlreadyExistsException() {
        super(HttpStatus.NOT_ACCEPTABLE, "Comment to this university already exists!");
    }
}
