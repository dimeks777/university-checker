package com.alevel.java.nix.universitychecker.config;

import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.service.JPAUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.security.SecureRandom;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@PropertySource(value={"classpath:application.yml"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JPAUserDetailsService userDetailsService;

    //For tests
    @Value("${open.urls.security.config}")
    private String[] antMatchers;

    @Autowired
    public SecurityConfig(JPAUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10, new SecureRandom());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .antMatchers(antMatchers).permitAll()
                .antMatchers(HttpMethod.POST, "/users").anonymous()
//                .antMatchers(antMatchers).permitAll()
                .antMatchers("/users/{id}/**").access("@userSecurity.hasUserId(authentication,#id)")
                .antMatchers("/user/{username}/**").access("@userSecurity.hasUserUsername(authentication,#username) or hasRole('MODERATOR')")
                .antMatchers("/admin/**").hasRole(UserRoles.ADMIN)
                .antMatchers("/moderators/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.POST, "/universities/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.PATCH, "/universities/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.PUT, "/universities/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.DELETE, "/universities/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.GET, "/universities/**").permitAll()
                .antMatchers(HttpMethod.POST, "/specialties/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.PUT, "/specialties/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.DELETE, "/specialties/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.GET, "/specialties/**").permitAll()
                .antMatchers(HttpMethod.POST, "/specialty_categories/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.PUT, "/specialty_categories/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.DELETE, "/specialty_categories/**").hasRole(UserRoles.MODERATOR)
                .antMatchers(HttpMethod.GET, "/specialty_categories/**").permitAll()
                .antMatchers("/registration").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/universities-find/**").permitAll()
                .requestMatchers(EndpointRequest.to(HealthEndpoint.class)).hasRole(UserRoles.MODERATOR)
//                .anyRequest().authenticated()

                .and()
                .httpBasic()
                .and()
                .userDetailsService(userDetailsService)
                .csrf().disable()
                .formLogin()
                .loginPage("/login").defaultSuccessUrl("/universities-find", true)
                .permitAll()
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .permitAll();
    }
}
