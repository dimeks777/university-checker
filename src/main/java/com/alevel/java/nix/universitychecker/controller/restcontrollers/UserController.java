package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.Comment;
import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.request.SaveCommentRequest;
import com.alevel.java.nix.universitychecker.entity.request.SaveRegularUserRequest;
import com.alevel.java.nix.universitychecker.entity.response.CommentResponse;
import com.alevel.java.nix.universitychecker.entity.response.UserResponse;
import com.alevel.java.nix.universitychecker.exception.NotEnoughRightsException;
import com.alevel.java.nix.universitychecker.service.CommentOperations;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserOperations userOperations;

    private final CommentOperations commentOperations;

    public UserController(UserOperations userOperations, CommentOperations commentOperations) {
        this.userOperations = userOperations;
        this.commentOperations = commentOperations;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserResponse> create(@RequestBody SaveRegularUserRequest request) {
        var saveRequest = request.toSaveRequest();
        var user = userOperations.create(saveRequest);
        return ResponseEntity
                .created(URI.create("/users/" + user.getId()))
                .body(UserResponse.fromUser(user));
    }

    @GetMapping("/{id}")
    public UserResponse getById(@PathVariable Integer id) {
        var user = userOperations.getById(id);
        return UserResponse.fromUser(user);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Integer id, @RequestBody SaveRegularUserRequest request) {
        userOperations.update(id, request.toSaveRequest());
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id) {
        userOperations.deleteById(id);
    }

    @GetMapping("/{id}/comments")
    public List<CommentResponse> getCommentsByUser(@PathVariable Integer id) {
        Set<Comment> comments = userOperations.getById(id).getComments();
        return comments.stream()
                .map(CommentResponse::fromComment)
                .collect(Collectors.toList());
    }

    @PostMapping("/{id}/comments")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CommentResponse> createComment(@PathVariable Integer id, @RequestParam Integer universityId,
                                                         @RequestBody SaveCommentRequest request) {
        var comment = commentOperations.create(id, universityId, request);
        return ResponseEntity
                .created(URI.create("/comments/" + comment.getId()))
                .body(CommentResponse.fromComment(comment));
    }

    @DeleteMapping("/{id}/comments/remove/{commentId}")
    public void removeComment(@PathVariable Integer id, @PathVariable UUID commentId) {
        var comment = commentOperations.getById(commentId);
        if (!comment.getUser().getId().equals(id) &&
                !comment.getUser().getRoles().contains(userOperations.getRoleByName(UserRoles.MODERATOR)))
            throw new NotEnoughRightsException();
        commentOperations.deleteById(commentId);
    }

    @PutMapping("/{id}/comments/edit")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Integer id, @RequestBody SaveCommentRequest request, @RequestParam UUID commentId) {
        commentOperations.update(commentId, request);
    }
}
