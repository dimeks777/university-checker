package com.alevel.java.nix.universitychecker.repository;

import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpecialtyCategoryRepository extends JpaRepository<SpecialtyCategory, Integer> {

    Optional<SpecialtyCategory> findByName(String name);

    boolean existsById(Integer id);

    void deleteById(Integer id);
}
