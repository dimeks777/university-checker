package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class SpecialtyNotFoundException extends UniversitiesCheckException {

    public SpecialtyNotFoundException(Integer id) {
        super(HttpStatus.NOT_FOUND, "Specialty with id " + id + " not found");
    }
}
