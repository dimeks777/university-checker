package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Role;
import com.alevel.java.nix.universitychecker.entity.User;
import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.request.SaveUserRequest;
import com.alevel.java.nix.universitychecker.exception.RoleNotFoundException;
import com.alevel.java.nix.universitychecker.exception.UserNotFoundException;
import com.alevel.java.nix.universitychecker.exception.UsernameAlreadyExistsException;
import com.alevel.java.nix.universitychecker.repository.RoleRepository;
import com.alevel.java.nix.universitychecker.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UserService implements UserOperations {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(SaveUserRequest request) {

        String username = request.getUsername();

        if (userRepository.existsByUsername(username)) {
            throw new UsernameAlreadyExistsException(username);
        }

        Set<Role> roles = getRolesByNames(request.getRoles());

        var user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(roles);

        return userRepository.save(user);
    }

    @Override
    public void update(Integer id, SaveUserRequest request) {

        var existingUser = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        String username = request.getUsername();

        if (!existingUser.getUsername().equals(username) && userRepository.existsByUsername(username)) {
            throw new UsernameAlreadyExistsException(username);
        }

        Set<Role> roles = getRolesByNames(request.getRoles());

        existingUser.setUsername(username);
        existingUser.setPassword(passwordEncoder.encode(request.getPassword()));
        existingUser.setRoles(roles);

        userRepository.save(existingUser);
    }

    @Override
    public void addModeratorRole(Integer id) {
        var existingUser = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        existingUser.getRoles().add(roleRepository.findByName(UserRoles.MODERATOR).orElseThrow(() -> new RoleNotFoundException(UserRoles.MODERATOR)));
        userRepository.save(existingUser);
    }

    @Override
    public void removeModeratorRole(Integer id) {
        var existingUser = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        var moderRole = roleRepository.findByName(UserRoles.MODERATOR).orElseThrow(() -> new RoleNotFoundException(UserRoles.MODERATOR));
        existingUser.getRoles().remove(moderRole);
        userRepository.save(existingUser);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public List<User> getAllByRole(String roleName) {
        var role = roleRepository.findByName(roleName).orElseThrow(() -> new RoleNotFoundException(roleName));
        return userRepository.findAllByRoles(role);
    }

    @Override
    public Role getRoleByName(String name) {
        return roleRepository
                .findByName(name)
                .orElseThrow(() -> new RoleNotFoundException(name));
    }

    @Override
    public User getById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    @Override
    public void deleteById(Integer id) {
        var user = getById(id);
        if (user != null) userRepository.deleteById(id);
    }

    private Set<Role> getRolesByNames(Set<String> roleNames) {
        Set<Role> roles = new HashSet<>(roleNames.size());

        for (String roleName : roleNames) {
            roles.add(getRoleByName(roleName));
        }
        return roles;
    }
}
