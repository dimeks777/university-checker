package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UniversitiesCheckException extends ResponseStatusException {

    public UniversitiesCheckException(HttpStatus status) {
        super(status);
    }

    public UniversitiesCheckException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public UniversitiesCheckException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }
}
