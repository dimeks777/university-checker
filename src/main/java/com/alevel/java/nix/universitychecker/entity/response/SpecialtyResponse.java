package com.alevel.java.nix.universitychecker.entity.response;

import com.alevel.java.nix.universitychecker.entity.Specialty;

public class SpecialtyResponse {
    private Integer id;

    private String name;

    private String specialtyCategoryName;

    public SpecialtyResponse(Integer id, String name, String specialtyCategoryName) {
        this.id = id;
        this.name = name;
        this.specialtyCategoryName = specialtyCategoryName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialtyCategoryName() {
        return specialtyCategoryName;
    }

    public void setSpecialtyCategoryName(String specialtyCategoryName) {
        this.specialtyCategoryName = specialtyCategoryName;
    }

    public static SpecialtyResponse fromSpecialty(Specialty specialty) {
        return new SpecialtyResponse(specialty.getId(), specialty.getName(),
                specialty.getSpecialtyCategory().getName());
    }
}
