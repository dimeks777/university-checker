package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;
import com.alevel.java.nix.universitychecker.entity.response.SpecialtyCategoryResponse;
import com.alevel.java.nix.universitychecker.entity.response.SpecialtyResponse;
import com.alevel.java.nix.universitychecker.service.SpecialtyCategoryOperations;
import com.alevel.java.nix.universitychecker.service.SpecialtyOperations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/specialty_categories")
public class SpecialtyCategoryController {

    @Value("${open.urls.security.config}")
    private String[] antMatchers;

    private final SpecialtyCategoryOperations specialtyCategoryOperations;

    private final SpecialtyOperations specialtyOperations;

    public SpecialtyCategoryController(SpecialtyCategoryOperations specialtyCategoryOperations, SpecialtyOperations specialtyOperations) {
        this.specialtyCategoryOperations = specialtyCategoryOperations;
        this.specialtyOperations = specialtyOperations;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<SpecialtyCategoryResponse> create(@RequestBody SaveSpecialtyCategoryRequest request) {
        var specialtyCategory = specialtyCategoryOperations.create(request);
        return ResponseEntity
                .created(URI.create("/specialty_categories/" + specialtyCategory.getId()))
                .body(SpecialtyCategoryResponse.fromSpecialtyCategory(specialtyCategory));
    }

    @GetMapping
    public List<SpecialtyCategoryResponse> getAll() {
        var specialtyCategories = specialtyCategoryOperations.getAll();
        return specialtyCategories.stream().map(SpecialtyCategoryResponse::fromSpecialtyCategory).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public SpecialtyCategoryResponse getById(@PathVariable Integer id) {
        var specialtyCategory = specialtyCategoryOperations.getById(id);
        return SpecialtyCategoryResponse.fromSpecialtyCategory(specialtyCategory);
    }

    @PutMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable String name, @RequestBody SaveSpecialtyCategoryRequest request) {
        specialtyCategoryOperations.update(name, request);
    }

    @DeleteMapping("/{name}")
    public void deleteByName(@PathVariable String name) {
        specialtyCategoryOperations.deleteByName(name);
    }


    @GetMapping("/{name}/specialties")
    public List<SpecialtyResponse> getAllSpecialtiesByCategory(@PathVariable String name) {
        List<Specialty> specialties = specialtyOperations.getAllSpecialtiesByCategoryName(name);
        return specialties.stream()
                .map(SpecialtyResponse::fromSpecialty)
                .collect(Collectors.toList());
    }

}
