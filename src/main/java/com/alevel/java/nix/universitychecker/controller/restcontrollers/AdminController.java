package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.response.UserResponse;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private final UserOperations userOperations;

    public AdminController(UserOperations userOperations) {
        this.userOperations = userOperations;
    }

    @GetMapping("/moderators")
    public List<UserResponse> getAllModerators() {
        return userOperations.getAllByRole(UserRoles.MODERATOR).stream().map(UserResponse::fromUser).collect(Collectors.toList());
    }

    @PatchMapping("/moderators/add")
    public void addModeratorRole(@RequestParam Integer id) {
        userOperations.addModeratorRole(id);
    }

    @PatchMapping("/moderators/remove")
    public void removeModeratorRole(@RequestParam Integer user) {
        userOperations.removeModeratorRole(user);
    }


}
