package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class UniversityAlreadyExistsException extends UniversitiesCheckException{

    public UniversityAlreadyExistsException(String name){
        super(HttpStatus.NOT_ACCEPTABLE,"University " + name + " already exists");
    }
}
