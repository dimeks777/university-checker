package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class UsernameAlreadyExistsException extends UniversitiesCheckException {

    public UsernameAlreadyExistsException(String username) {
        super(HttpStatus.NOT_ACCEPTABLE, "User " + username + "already exists");
    }

}
