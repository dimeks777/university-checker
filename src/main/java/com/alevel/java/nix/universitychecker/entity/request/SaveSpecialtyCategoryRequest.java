package com.alevel.java.nix.universitychecker.entity.request;

public class SaveSpecialtyCategoryRequest {

    private String name;

    public SaveSpecialtyCategoryRequest(String name) {
        this.name = name;
    }

    public SaveSpecialtyCategoryRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
