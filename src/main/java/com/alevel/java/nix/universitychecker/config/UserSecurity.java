package com.alevel.java.nix.universitychecker.config;

import com.alevel.java.nix.universitychecker.entity.User;
import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.exception.UserNotFoundException;
import com.alevel.java.nix.universitychecker.repository.UserRepository;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component("userSecurity")
public class UserSecurity {

    private final UserRepository userRepository;

    private final UserOperations userOperations;

    public UserSecurity(UserRepository userRepository, UserOperations userOperations) {
        this.userRepository = userRepository;
        this.userOperations = userOperations;
    }

    public boolean hasUserId(Authentication authentication, Integer id) {
        try {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            User user = userRepository.findByUsername(userDetails.getUsername())
                    .orElseThrow(() -> new UserNotFoundException(userDetails.getUsername()));
            var changingUser = userOperations.getById(id);
            return user.getId().equals(id) || ((user.getRoles().contains(userOperations.getRoleByName(UserRoles.MODERATOR))
                    && !changingUser.getRoles().contains(userOperations.getRoleByName(UserRoles.MODERATOR))) ||
                    user.getRoles().contains(userOperations.getRoleByName(UserRoles.ADMIN)));
        } catch (Exception e) {
            return false;
        }

    }

    public boolean hasUserUsername(Authentication authentication, String username) {
        try {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            User user = userRepository.findByUsername(userDetails.getUsername())
                    .orElseThrow(() -> new UserNotFoundException(userDetails.getUsername()));
            var changingUser = userOperations.getByUsername(username);
            return user.getUsername().equals(username) || ((user.getRoles().contains(userOperations.getRoleByName(UserRoles.MODERATOR))
                    && !changingUser.getRoles().contains(userOperations.getRoleByName(UserRoles.MODERATOR))) ||
                    user.getRoles().contains(userOperations.getRoleByName(UserRoles.ADMIN)));
        } catch (Exception e) {
            return false;
        }
    }


}
