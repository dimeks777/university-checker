package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class NotEnoughRightsException extends UniversitiesCheckException {

    public NotEnoughRightsException() {
        super(HttpStatus.FORBIDDEN, "You don't have rights to do this");
    }
}
