package com.alevel.java.nix.universitychecker.validate;

public class DataValidator {

    public static boolean checkPoints(double points){
        return points >= 0 && points <= 100;
    }
}
