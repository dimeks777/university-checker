package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyRequest;

import java.util.List;

public interface SpecialtyOperations {

    Specialty create(SaveSpecialtyRequest request);

    void update(Integer id, SaveSpecialtyRequest request);

    Specialty getById(Integer id);

    void deleteById(Integer id);

    void deleteAll();

    List<Specialty> getAll();

    List<Specialty> getAllSpecialtiesByCategoryName(String categoryName);
}
