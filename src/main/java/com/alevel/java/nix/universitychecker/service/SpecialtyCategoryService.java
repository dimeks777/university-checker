package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;
import com.alevel.java.nix.universitychecker.exception.SpecialtyCategoryNotFoundException;
import com.alevel.java.nix.universitychecker.repository.SpecialtyCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SpecialtyCategoryService implements SpecialtyCategoryOperations {

    private SpecialtyCategoryRepository specialtyCategoryRepository;

    @Autowired
    public void setSpecialtyCategoryRepository(SpecialtyCategoryRepository specialtyCategoryRepository) {
        this.specialtyCategoryRepository = specialtyCategoryRepository;
    }

    public SpecialtyCategoryService(SpecialtyCategoryRepository specialtyCategoryRepository) {
        this.specialtyCategoryRepository = specialtyCategoryRepository;
    }

    public SpecialtyCategoryService() {
    }

    @Override
    public SpecialtyCategory create(SaveSpecialtyCategoryRequest request) {
        var specialtyCategory = new SpecialtyCategory();
        specialtyCategory.setName(request.getName());
        return specialtyCategoryRepository.save(specialtyCategory);
    }

    @Override
    public void update(String name, SaveSpecialtyCategoryRequest request) {
        var existingSpecialtyCategory = getByName(name);
        existingSpecialtyCategory.setName(request.getName());
        specialtyCategoryRepository.save(existingSpecialtyCategory);
    }

    @Override
    public SpecialtyCategory getById(Integer id) {
        return specialtyCategoryRepository.findById(id).orElseThrow(() -> new SpecialtyCategoryNotFoundException(id));
    }

    @Override
    public SpecialtyCategory getByName(String name) {
        return specialtyCategoryRepository.findByName(name).orElseThrow(() -> new SpecialtyCategoryNotFoundException(name));
    }

    @Override
    public void deleteByName(String name) {
        var category = getByName(name);
        specialtyCategoryRepository.deleteById(category.getId());
    }

    @Override
    public List<SpecialtyCategory> getAll() {
        return specialtyCategoryRepository.findAll();
    }

    @Override
    public void deleteAll() {
        specialtyCategoryRepository.deleteAll();
    }
}
