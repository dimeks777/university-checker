package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.request.SaveCommentRequest;
import com.alevel.java.nix.universitychecker.entity.response.CommentResponse;
import com.alevel.java.nix.universitychecker.service.CommentOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/comments")
public class CommentController {

    private final CommentOperations commentOperations;

    public CommentController(CommentOperations commentOperations) {
        this.commentOperations = commentOperations;
    }

    @GetMapping("/{id}")
    public CommentResponse getById(@PathVariable UUID id) {
        var comment = commentOperations.getById(id);
        return CommentResponse.fromComment(comment);
    }

    @GetMapping
    public List<CommentResponse> getAllByUserId(@RequestParam Integer user) {
        var commentList = commentOperations.getAllByUserId(user);
        return commentList.stream().map(CommentResponse::fromComment).collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable UUID id, @RequestBody SaveCommentRequest request) {
        commentOperations.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable UUID id) {
        commentOperations.deleteById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CommentResponse> post(@RequestBody SaveCommentRequest request, @RequestParam Integer userId, @RequestParam Integer universityId) {
        var comment =  commentOperations.create(userId, universityId, request);
        return ResponseEntity
                .created(URI.create("/comments/" + comment.getId()))
                .body(CommentResponse.fromComment(comment));
    }
}
