package com.alevel.java.nix.universitychecker.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "universities")
public class University {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String name;

    @ManyToMany
    @JoinTable(
            name = "specialties_of_universities",
            inverseJoinColumns = @JoinColumn(name = "specialty_id", referencedColumnName = "id"),
            joinColumns = @JoinColumn(name = "university_id", referencedColumnName = "id")
    )
    private Set<Specialty> specialties;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "university")
    private List<Comment> comments;

    private Double officialPoints;

    private Double userPoints;

    @OneToOne(cascade = CascadeType.ALL)
    private Location location;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Specialty> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(Set<Specialty> specialties) {
        this.specialties = specialties;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Double getOfficialPoints() {
        return officialPoints;
    }

    public void setOfficialPoints(Double officialPoints) {
        this.officialPoints = officialPoints;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Double getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(Double userPoints) {
        this.userPoints = userPoints;
    }
}
