package com.alevel.java.nix.universitychecker.repository;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface UniversityRepository extends JpaRepository<University, Integer> {

    List<University> findAllByLocation_Country(String country);

    List<University> findAllByLocation_CountryAndLocation_City(String country, String city);

    boolean existsByName(String name);

    List<University> findAllByOrderByOfficialPointsDesc();

    List<University> findAllByLocation_CountryOrderByOfficialPointsDesc(String country);

    List<University> findAllByLocation_CountryAndLocation_CityOrderByOfficialPointsDesc(String country, String city);

    List<University> findAllByUserPointsNotNullOrderByUserPointsDesc();

    List<University> findAllByUserPointsNotNullAndLocation_CountryOrderByUserPointsDesc(String country);

    List<University> findAllByUserPointsNotNullAndLocation_CountryAndLocation_CityOrderByUserPointsDesc(String country, String city);


}
