package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public class CommentNotFoundException extends UniversitiesCheckException {

    public CommentNotFoundException(UUID commentId) {
        super(HttpStatus.NOT_FOUND, "Comment with id " + commentId + " not found");
    }

}
