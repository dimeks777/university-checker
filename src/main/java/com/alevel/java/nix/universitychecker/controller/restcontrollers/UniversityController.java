package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.University;
import com.alevel.java.nix.universitychecker.entity.request.SaveUniversityRequest;
import com.alevel.java.nix.universitychecker.entity.response.CommentResponse;
import com.alevel.java.nix.universitychecker.entity.response.SpecialtyResponse;
import com.alevel.java.nix.universitychecker.entity.response.UniversityResponse;
import com.alevel.java.nix.universitychecker.service.CommentOperations;
import com.alevel.java.nix.universitychecker.service.UniversityOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/universities")
public class UniversityController {

    private final UniversityOperations universityOperations;

    private final CommentOperations commentOperations;

    public UniversityController(UniversityOperations universityOperations, CommentOperations commentOperations) {
        this.universityOperations = universityOperations;
        this.commentOperations = commentOperations;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UniversityResponse> create(@RequestBody SaveUniversityRequest request) {
        var university = universityOperations.create(request);
        return ResponseEntity
                .created(URI.create("/universities/" + university.getId()))
                .body(UniversityResponse.fromUniversity(university));
    }

    @GetMapping("/{id}")
    public UniversityResponse getById(@PathVariable Integer id) {
        var university = universityOperations.getById(id);
        return UniversityResponse.fromUniversity(university);
    }

    @GetMapping("/{id}/comments")
    public List<CommentResponse> getCommentsByUniversityId(@PathVariable Integer id) {
        var comments = commentOperations.getAllByUniversityId(id);
        return comments.stream().map(CommentResponse::fromComment).collect(Collectors.toList());
    }

    @GetMapping("/{id}/specialties")
    public List<SpecialtyResponse> getSpecialtiesByUniversityId(@PathVariable Integer id) {
        var specialties = universityOperations.getById(id).getSpecialties();
        return specialties.stream().map(SpecialtyResponse::fromSpecialty).collect(Collectors.toList());
    }

    @GetMapping("/find")
    public List<UniversityResponse> findByLocation(@RequestParam(required = false) String rating, @RequestParam(required = false) String country, @RequestParam(required = false) String city, @RequestParam(required = false) Integer... specialties) {
        List<University> universities = null;
        if (rating == null && country == null && city == null) {

            universities = universityOperations.getAll();

        } else if (rating != null && country == null && city == null) {

            if (rating.equalsIgnoreCase("official")) {
                universities = universityOperations.getGlobalOfficialUniversitiesRating();
            } else if (rating.equalsIgnoreCase("user")) {
                universities = universityOperations.getGlobalUserUniversitiesRating();
            }


        } else if (rating != null && country != null && city == null) {

            if (rating.equalsIgnoreCase("official")) {
                universities = universityOperations.getCountryOfficialUniversitiesRating(country);
            } else if (rating.equalsIgnoreCase("user")) {
                universities = universityOperations.getCountryUserUniversitiesRating(country);
            }

        } else if (rating != null && country != null) {

            if (rating.equalsIgnoreCase("official")) {
                universities = universityOperations.getLocalOfficialUniversitiesRating(country, city);
            } else if (rating.equalsIgnoreCase("user")) {
                universities = universityOperations.getLocalUserUniversitiesRating(country, city);
            }

        } else if (rating == null && country != null && city == null) {

            universities = universityOperations.getAllByCountry(country);

        } else if (rating == null && country != null) {

            universities = universityOperations.getAllByCountryAndCity(country, city);
        }

        if (universities != null && specialties != null) {

            Set<Integer> set = new TreeSet<>(Arrays.asList(specialties));
            Set<Specialty> specialtiesSet = universityOperations.getAllExistingSpecialties(set);
            return universities.stream().filter(e -> e.getSpecialties().containsAll(specialtiesSet)).map(UniversityResponse::fromUniversity).collect(Collectors.toList());

        } else if (universities != null) {
            return universities.stream().map(UniversityResponse::fromUniversity).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Integer id, @RequestBody SaveUniversityRequest request) {
        universityOperations.update(id, request);
    }

    @PatchMapping("/{id}/specialties")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSpecialties(@PathVariable Integer id, @RequestParam Integer... specialties) {
        universityOperations.updateSpecialties(id, Set.of(specialties));
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePoints(@PathVariable Integer id, @RequestParam Double points) {
        universityOperations.updatePoints(id, points);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id) {
        universityOperations.deleteById(id);
    }


}
