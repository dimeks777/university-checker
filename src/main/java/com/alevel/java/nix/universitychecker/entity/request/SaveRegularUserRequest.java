package com.alevel.java.nix.universitychecker.entity.request;

import com.alevel.java.nix.universitychecker.entity.UserRoles;

import java.util.Collections;

public class SaveRegularUserRequest {

    private String username;

    private String password;

    public SaveRegularUserRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public SaveRegularUserRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SaveUserRequest toSaveRequest() {
        var saveRequest = new SaveUserRequest();
        saveRequest.setUsername(username);
        saveRequest.setPassword(password);
        saveRequest.setRoles(Collections.singleton(UserRoles.USER));
        return saveRequest;
    }

}
