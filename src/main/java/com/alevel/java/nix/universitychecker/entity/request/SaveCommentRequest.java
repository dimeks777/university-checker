package com.alevel.java.nix.universitychecker.entity.request;

public class SaveCommentRequest {

    private String text;

    private Double points;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }
}
