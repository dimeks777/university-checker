package com.alevel.java.nix.universitychecker.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "specialties")
public class Specialty {

    @Id
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "specialties")
    private Set<University> universities;

    @OneToOne
    private SpecialtyCategory specialtyCategory;

    public Specialty() {
    }

    public Specialty(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Specialty(Integer id, String name, SpecialtyCategory specialtyCategory) {
        this.id = id;
        this.name = name;
        this.specialtyCategory = specialtyCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SpecialtyCategory getSpecialtyCategory() {
        return specialtyCategory;
    }

    public void setSpecialtyCategory(SpecialtyCategory specialtyCategory) {
        this.specialtyCategory = specialtyCategory;
    }

    public Set<University> getUniversities() {
        return universities;
    }

    public void setUniversities(Set<University> universities) {
        this.universities = universities;
    }


    @Override
    public String toString() {
        return id +
                " " + name +
                ", field of knowledge: " + specialtyCategory.getName();
    }
}
