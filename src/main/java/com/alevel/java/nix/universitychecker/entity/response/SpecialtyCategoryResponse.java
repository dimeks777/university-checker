package com.alevel.java.nix.universitychecker.entity.response;

import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;

public class SpecialtyCategoryResponse {

    private Integer id;

    private String name;

    public SpecialtyCategoryResponse(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public static SpecialtyCategoryResponse fromSpecialtyCategory(SpecialtyCategory specialtyCategory) {
        return new SpecialtyCategoryResponse(specialtyCategory.getId(),specialtyCategory.getName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
