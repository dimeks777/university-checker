package com.alevel.java.nix.universitychecker.controller;

import com.alevel.java.nix.universitychecker.entity.User;
import com.alevel.java.nix.universitychecker.entity.request.SaveRegularUserRequest;
import com.alevel.java.nix.universitychecker.repository.UserRepository;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    private final UserOperations userOperations;

    private final UserRepository userRepository;

    public UserRegistrationController(UserOperations userOperations, UserRepository userRepository) {
        this.userOperations = userOperations;
        this.userRepository = userRepository;
    }

    @ModelAttribute("user")
    public User userRegistrationDto() {
        return new User();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") User userDto,
                                      BindingResult result) {

        userRepository.findByUsername(userDto.getUsername()).ifPresent(existing -> result.rejectValue("username", null, "There is already an account registered with that username"));

        if (result.hasErrors()) {
            return "registration";
        }

        userOperations.create(new SaveRegularUserRequest(userDto.getUsername(),userDto.getPassword()).toSaveRequest());
        return "redirect:/login";
    }

}