package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Comment;
import com.alevel.java.nix.universitychecker.entity.request.SaveCommentRequest;
import com.alevel.java.nix.universitychecker.exception.CommentAlreadyExistsException;
import com.alevel.java.nix.universitychecker.exception.CommentNotFoundException;
import com.alevel.java.nix.universitychecker.exception.UniversityNotFoundException;
import com.alevel.java.nix.universitychecker.exception.UserNotFoundException;
import com.alevel.java.nix.universitychecker.repository.CommentRepository;
import com.alevel.java.nix.universitychecker.repository.UniversityRepository;
import com.alevel.java.nix.universitychecker.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class CommentService implements CommentOperations {

    private final CommentRepository commentRepository;

    private final UserRepository userRepository;

    private final UniversityRepository universityRepository;

    private final UniversityOperations universityOperations;

    public CommentService(CommentRepository commentRepository, UserRepository userRepository, UniversityRepository universityRepository, UniversityOperations universityOperations) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.universityRepository = universityRepository;
        this.universityOperations = universityOperations;
    }

    @Override
    public Comment create(Integer userId, Integer universityId, SaveCommentRequest request) {
        var user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        var university = universityRepository.findById(universityId).orElseThrow(() -> new UniversityNotFoundException(universityId));
        var comment = new Comment();
        comment.setText(request.getText());
        comment.setUser(user);
        comment.setPoints(request.getPoints());
        comment.setUniversity(university);
        if (!user.getComments().add(comment)) {
            throw new CommentAlreadyExistsException();
        }
        university.getComments().add(comment);
        Double currentUserPoints = university.getUserPoints();
        if (currentUserPoints == null) {
            university.setUserPoints(request.getPoints());
        } else {
            university.setUserPoints((currentUserPoints + request.getPoints()) / 2);
        }
        return commentRepository.save(comment);
    }

    @Override
    public void update(UUID id, SaveCommentRequest request) {
        var existingComment = getById(id);
        existingComment.setText(request.getText());
        existingComment.setPoints(request.getPoints());
        commentRepository.save(existingComment);
        var university = universityOperations.getById(existingComment.getUniversity().getId());
        university.setUserPoints(universityOperations.getAveragePoints(university.getId()));
        universityRepository.save(university);
    }

    @Override
    public Comment getById(UUID id) {
        return commentRepository.findById(id).orElseThrow(() -> new CommentNotFoundException(id));
    }

    @Override
    public void deleteById(UUID id) {
        var comment = getById(id);
        commentRepository.deleteById(comment.getId());
    }

    @Override
    public void deleteAll() {
        commentRepository.deleteAll();
    }

    @Override
    public List<Comment> getAllByUniversityId(Integer universityId) {
        if (!universityRepository.existsById(universityId)) {
            throw new UniversityNotFoundException(universityId);
        }
        return commentRepository.findAllByUniversityId(universityId);
    }

    @Override
    public List<Comment> getAllByUserId(Integer userId) {
        if (!userRepository.existsById(userId)) {
            throw new UserNotFoundException(userId);
        }
        return commentRepository.findAllByUserId(userId);
    }
}
