package com.alevel.java.nix.universitychecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityCheckerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniversityCheckerApplication.class, args);
    }

}
