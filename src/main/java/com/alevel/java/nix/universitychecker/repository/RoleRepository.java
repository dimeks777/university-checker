package com.alevel.java.nix.universitychecker.repository;

import com.alevel.java.nix.universitychecker.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(String name);
//    List<Role> findAllByName(Set<String> names);
}
