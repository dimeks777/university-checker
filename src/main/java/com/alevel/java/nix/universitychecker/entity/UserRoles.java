package com.alevel.java.nix.universitychecker.entity;

public class UserRoles {

    public static final String ADMIN = "ADMIN";

    public static final String MODERATOR = "MODERATOR";

    public static final String USER = "USER";
}
