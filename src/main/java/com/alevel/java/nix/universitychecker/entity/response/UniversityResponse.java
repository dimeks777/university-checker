package com.alevel.java.nix.universitychecker.entity.response;

import com.alevel.java.nix.universitychecker.entity.University;

public class UniversityResponse {

    private Integer id;

    private String name;

    private Double officialPoints;

    private Double userPoints;

    private String location;

    public static UniversityResponse fromUniversity(University university) {
        return new UniversityResponse(
                university.getId(),
                university.getName(),
                university.getOfficialPoints(),
                university.getUserPoints(),
                university.getLocation().toString()
        );
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UniversityResponse(Integer id, String name, Double officialPoints, Double userPoints, String location) {
        this.id = id;
        this.name = name;
        this.officialPoints = officialPoints;
        this.userPoints = userPoints;
        this.location = location;
    }

    public UniversityResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getOfficialPoints() {
        return officialPoints;
    }

    public void setOfficialPoints(Double officialPoints) {
        this.officialPoints = officialPoints;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(Double userPoints) {
        this.userPoints = userPoints;
    }
}
