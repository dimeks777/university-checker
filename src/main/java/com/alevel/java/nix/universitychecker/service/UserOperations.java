package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Role;
import com.alevel.java.nix.universitychecker.entity.User;
import com.alevel.java.nix.universitychecker.entity.request.SaveUserRequest;

import java.util.List;

public interface UserOperations {

    User create(SaveUserRequest request);

    void update(Integer id, SaveUserRequest request);

    void addModeratorRole(Integer id);

    void removeModeratorRole(Integer id);

    List<User> getAll();

    void deleteAll();

    List<User> getAllByRole(String roleName);

    Role getRoleByName(String name);

    User getById(Integer id);

    User getByUsername(String username);

    void deleteById(Integer id);

}
