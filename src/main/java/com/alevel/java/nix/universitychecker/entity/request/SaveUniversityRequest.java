package com.alevel.java.nix.universitychecker.entity.request;

import com.alevel.java.nix.universitychecker.entity.Location;

import java.util.Set;

public class SaveUniversityRequest {

    private String name;

    private Set<Integer> specialtyIds;

    private Double officialPoints;

    private Location location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getOfficialPoints() {
        return officialPoints;
    }

    public void setOfficialPoints(Double officialPoints) {
        this.officialPoints = officialPoints;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<Integer> getSpecialtyIds() {
        return specialtyIds;
    }

    public void setSpecialtyIds(Set<Integer> specialtyIds) {
        this.specialtyIds = specialtyIds;
    }

    public SaveUniversityRequest(String name, Set<Integer> specialtyIds, Double officialPoints, Location location) {
        this.name = name;
        this.specialtyIds = specialtyIds;
        this.officialPoints = officialPoints;
        this.location = location;
    }

    public SaveUniversityRequest() {
    }
}
