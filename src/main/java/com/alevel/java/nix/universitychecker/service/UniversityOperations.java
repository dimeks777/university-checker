package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Location;
import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.University;
import com.alevel.java.nix.universitychecker.entity.request.SaveUniversityRequest;

import java.util.List;
import java.util.Set;

public interface UniversityOperations {

    University create(SaveUniversityRequest request);

    void update(Integer id, SaveUniversityRequest request);

    void updatePoints(Integer id, Double points);

    void updateSpecialties(Integer id, Set<Integer> specialties);

    University getById(Integer id);

    void deleteById(Integer id);

    void deleteAll();

    List<University> getAll();

    Double getAveragePoints(Integer id);

    Set<Specialty> getAllExistingSpecialties(Set<Integer> specialtyIds);

    List<University> getAllByCountry(String country);

    List<University> getAllByCountryAndCity(String country, String city);

    List<University> getGlobalOfficialUniversitiesRating();

    List<University> getCountryOfficialUniversitiesRating(String country);

    List<University> getLocalOfficialUniversitiesRating(String country, String city);

    List<University> getGlobalUserUniversitiesRating();

    List<University> getCountryUserUniversitiesRating(String country);

    List<University> getLocalUserUniversitiesRating(String country, String city);

}
