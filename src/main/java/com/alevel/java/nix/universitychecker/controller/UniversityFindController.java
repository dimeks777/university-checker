package com.alevel.java.nix.universitychecker.controller;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.University;
import com.alevel.java.nix.universitychecker.service.UniversityOperations;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Controller
public class UniversityFindController {

    private final UniversityOperations universityOperations;

    private final UserOperations userOperations;

    public UniversityFindController(UniversityOperations universityOperations, UserOperations userOperations) {
        this.universityOperations = universityOperations;
        this.userOperations = userOperations;
    }

    @GetMapping("/universities-find")
    public String findUniversities(Model model,
                                   @RequestParam(required = false) String country,
                                   @RequestParam(required = false) String city,
                                   @RequestParam(required = false) Integer... specialties) {
        List<University> universities = findByParams(country, city, specialties);
        model.addAttribute("universities", universities);
        return "universities/universitylist";
    }

    @GetMapping("/universities-find/{id}")
    public String getUniversityInfo(Model model, @PathVariable Integer id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        var user = userOperations.getByUsername(authentication.getName());
        var university = universityOperations.getById(id);
        model.addAttribute("university", university);
        model.addAttribute("user", user);
        return "universities/universityInfo";
    }

    public List<University> findByParams(String country, String city, Integer... specialties) {

        List<University> universities = null;

        boolean countryNotEmpty = checkStringNullOrEmpty(country);
        boolean cityNotEmpty = checkStringNullOrEmpty(city);

        if (!countryNotEmpty && !cityNotEmpty) {

            universities = universityOperations.getAll();

        } else if (countryNotEmpty && !cityNotEmpty) {

            universities = universityOperations.getAllByCountry(country);

        } else if (countryNotEmpty) {

            universities = universityOperations.getAllByCountryAndCity(country, city);
        }

        if (universities != null && specialties != null) {

            Set<Integer> set = new TreeSet<>(Arrays.asList(specialties));
            Set<Specialty> specialtiesSet = universityOperations.getAllExistingSpecialties(set);
            return universities.stream().filter(e -> e.getSpecialties().containsAll(specialtiesSet)).collect(Collectors.toList());

        } else return universities;


    }

    public boolean checkStringNullOrEmpty(String str) {
        return (str != null) && !str.isEmpty();
    }


}
