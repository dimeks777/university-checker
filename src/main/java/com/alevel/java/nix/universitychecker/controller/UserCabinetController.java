package com.alevel.java.nix.universitychecker.controller;

import com.alevel.java.nix.universitychecker.exception.NotEnoughRightsException;
import com.alevel.java.nix.universitychecker.repository.UniversityRepository;
import com.alevel.java.nix.universitychecker.service.CommentOperations;
import com.alevel.java.nix.universitychecker.service.UniversityOperations;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@Controller
public class UserCabinetController {

    private final UserOperations userOperations;

    private final CommentOperations commentOperations;

    private final UniversityOperations universityOperations;

    private final UniversityRepository universityRepository;

    public UserCabinetController(UserOperations userOperations, CommentOperations commentOperations, UniversityOperations universityOperations, UniversityRepository universityRepository) {
        this.userOperations = userOperations;
        this.commentOperations = commentOperations;
        this.universityOperations = universityOperations;
        this.universityRepository = universityRepository;
    }

    @GetMapping("/user/{username}")
    public String getUserInfo(@PathVariable String username, Model model) {
        var user = userOperations.getByUsername(username);
        model.addAttribute("user", user);
        return "user/userinfo";
    }

    @PostMapping("/user/{username}/comments/remove/{commentId}")
    public String deleteComment(@PathVariable String username, @PathVariable UUID commentId, Model model) {
        var comment = commentOperations.getById(commentId);
        var university = universityOperations.getById(comment.getUniversity().getId());
        var user = userOperations.getByUsername(username);
        if (comment.getUser().getUsername().equals(username)) {
            commentOperations.deleteById(commentId);
            Double points = universityOperations.getAveragePoints(university.getId());
            university.setUserPoints(points == 0 ? null : points);
            universityRepository.save(university);
        } else {
            throw new NotEnoughRightsException();
        }
        model.addAttribute("user", user);
        return "user/userinfo";
    }

}
