package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Comment;
import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.University;
import com.alevel.java.nix.universitychecker.entity.request.SaveUniversityRequest;
import com.alevel.java.nix.universitychecker.exception.UniversityAlreadyExistsException;
import com.alevel.java.nix.universitychecker.exception.UniversityNotFoundException;
import com.alevel.java.nix.universitychecker.repository.CommentRepository;
import com.alevel.java.nix.universitychecker.repository.LocationRepository;
import com.alevel.java.nix.universitychecker.repository.SpecialtyRepository;
import com.alevel.java.nix.universitychecker.repository.UniversityRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
@Transactional
public class UniversityService implements UniversityOperations {

    private final UniversityRepository universityRepository;

    private final SpecialtyRepository specialtyRepository;

    private final LocationRepository locationRepository;

    private final CommentRepository commentRepository;

    public UniversityService(UniversityRepository universityRepository, SpecialtyRepository specialtyRepository, LocationRepository locationRepository, CommentRepository commentRepository) {
        this.universityRepository = universityRepository;
        this.specialtyRepository = specialtyRepository;
        this.locationRepository = locationRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public University create(SaveUniversityRequest request) {
        var university = new University();
        if (universityRepository.existsByName(request.getName()))
            throw new UniversityAlreadyExistsException(request.getName());
        university.setName(request.getName());
        university.setOfficialPoints(request.getOfficialPoints());
        var existingLocation = locationRepository.findByCity(request.getLocation().getCity());
        if (existingLocation != null) {
            university.setLocation(existingLocation);
        } else {
            university.setLocation(request.getLocation());
        }
        university.setSpecialties(getAllExistingSpecialties(request.getSpecialtyIds()));
        return universityRepository.save(university);
    }

    @Override
    public void update(Integer id, SaveUniversityRequest request) {
        var existingUniversity = getById(id);
        existingUniversity.setName(request.getName());
        existingUniversity.setSpecialties(getAllExistingSpecialties(request.getSpecialtyIds()));
        existingUniversity.setOfficialPoints(request.getOfficialPoints());
        existingUniversity.setLocation(request.getLocation());
        universityRepository.save(existingUniversity);
    }

    @Override
    public void updatePoints(Integer id, Double points) {
        var existingUniversity = getById(id);
        existingUniversity.setOfficialPoints(points);
        universityRepository.save(existingUniversity);
    }

    @Override
    public void updateSpecialties(Integer id, Set<Integer> specialties) {
        var existingUniversity = getById(id);
        existingUniversity.setSpecialties(getAllExistingSpecialties(specialties));
        universityRepository.save(existingUniversity);
    }

    @Override
    public University getById(Integer id) {
        return universityRepository.findById(id).orElseThrow(() -> new UniversityNotFoundException(id));
    }

    @Override
    public void deleteById(Integer id) {
        var university = getById(id);
        if (university != null) universityRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        universityRepository.deleteAll();
    }

    @Override
    public List<University> getAll() {
        return universityRepository.findAll();
    }

    @Override
    public Double getAveragePoints(Integer id) {
        var comments = commentRepository.findAllByUniversityId(id);
        if (comments == null) {
            return null;
        }
        return comments.stream().mapToDouble(Comment::getPoints).summaryStatistics().getAverage();
    }

    public Specialty getSpecialtyById(Integer id) {
        return specialtyRepository.findById(id).orElseThrow(() -> new UniversityNotFoundException(id));
    }

    @Override
    public Set<Specialty> getAllExistingSpecialties(Set<Integer> specialtyIds) {
        Set<Specialty> existingSpecialties = new HashSet<>(specialtyIds.size());
        for (Integer id : specialtyIds) {
            existingSpecialties.add(getSpecialtyById(id));
        }
        return existingSpecialties;
    }

    @Override
    public List<University> getAllByCountry(String country) {
        return universityRepository.findAllByLocation_Country(country);
    }

    @Override
    public List<University> getAllByCountryAndCity(String country, String city) {
        return universityRepository.findAllByLocation_CountryAndLocation_City(country, city);
    }

    @Override
    public List<University> getGlobalOfficialUniversitiesRating() {
        return universityRepository.findAllByOrderByOfficialPointsDesc();
    }

    @Override
    public List<University> getCountryOfficialUniversitiesRating(String country) {
        return universityRepository.findAllByLocation_CountryOrderByOfficialPointsDesc(country);
    }

    @Override
    public List<University> getLocalOfficialUniversitiesRating(String country, String city) {
        return universityRepository.findAllByLocation_CountryAndLocation_CityOrderByOfficialPointsDesc(country, city);
    }

    @Override
    public List<University> getGlobalUserUniversitiesRating() {
        return universityRepository.findAllByUserPointsNotNullOrderByUserPointsDesc();
    }

    @Override
    public List<University> getCountryUserUniversitiesRating(String country) {
        return universityRepository.findAllByUserPointsNotNullAndLocation_CountryOrderByUserPointsDesc(country);
    }

    @Override
    public List<University> getLocalUserUniversitiesRating(String country, String city) {
        return universityRepository.findAllByUserPointsNotNullAndLocation_CountryAndLocation_CityOrderByUserPointsDesc(country, city);
    }

}
