package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends UniversitiesCheckException {

    public RoleNotFoundException(String name) {
        super(HttpStatus.NOT_FOUND, "Role with name " + name + " not found");
    }
}
