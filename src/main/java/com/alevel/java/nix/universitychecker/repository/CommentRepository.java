package com.alevel.java.nix.universitychecker.repository;

import com.alevel.java.nix.universitychecker.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CommentRepository extends JpaRepository<Comment, UUID> {

    List<Comment> findAllByUniversityId(Integer universityId);

    List<Comment> findAllByUserId(Integer userId);

}
