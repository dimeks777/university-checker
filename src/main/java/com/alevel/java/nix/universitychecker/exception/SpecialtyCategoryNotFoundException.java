package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class SpecialtyCategoryNotFoundException extends UniversitiesCheckException {

    public SpecialtyCategoryNotFoundException(Integer id) {
        super(HttpStatus.NOT_FOUND, "Specialty category with id " + id + " not found");
    }

    public SpecialtyCategoryNotFoundException(String name) {
        super(HttpStatus.NOT_FOUND, "Specialty category with name " + name + " not found");
    }
}
