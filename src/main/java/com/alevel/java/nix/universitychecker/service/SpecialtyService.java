package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyRequest;
import com.alevel.java.nix.universitychecker.exception.SpecialtyCategoryNotFoundException;
import com.alevel.java.nix.universitychecker.exception.SpecialtyNotFoundException;
import com.alevel.java.nix.universitychecker.repository.SpecialtyCategoryRepository;
import com.alevel.java.nix.universitychecker.repository.SpecialtyRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SpecialtyService implements SpecialtyOperations {

    private final SpecialtyRepository specialtyRepository;

    private final SpecialtyCategoryRepository specialtyCategoryRepository;

    public SpecialtyService(SpecialtyRepository specialtyRepository, SpecialtyCategoryRepository specialtyCategoryRepository) {
        this.specialtyRepository = specialtyRepository;
        this.specialtyCategoryRepository = specialtyCategoryRepository;
    }

    @Override
    public Specialty create(SaveSpecialtyRequest request) {
        var specialty = new Specialty();
        specialty.setId(request.getId());
        specialty.setName(request.getName());
        specialty.setSpecialtyCategory(specialtyCategoryRepository.findByName(request.getSpecialtyCategoryName())
                .orElseThrow(() -> new SpecialtyCategoryNotFoundException(request.getSpecialtyCategoryName())));
        return specialtyRepository.save(specialty);
    }

    @Override
    public void update(Integer id, SaveSpecialtyRequest request) {
        var existingSpecialty = getById(id);
        existingSpecialty.setName(request.getName());
        existingSpecialty.setSpecialtyCategory(specialtyCategoryRepository.findByName(request.getSpecialtyCategoryName())
                .orElseThrow(() -> new SpecialtyCategoryNotFoundException(request.getSpecialtyCategoryName())));
    }

    @Override
    public Specialty getById(Integer id) {
        return specialtyRepository.findById(id).orElseThrow(() -> new SpecialtyNotFoundException(id));
    }

    @Override
    public void deleteById(Integer id) {
        var specialty = getById(id);
        if (specialty != null) specialtyRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        specialtyRepository.deleteAll();
    }

    @Override
    public List<Specialty> getAll() {
        return specialtyRepository.findAll();
    }

    @Override
    public List<Specialty> getAllSpecialtiesByCategoryName(String categoryName) {
        return specialtyRepository.findAllBySpecialtyCategory_Name(categoryName);
    }
}
