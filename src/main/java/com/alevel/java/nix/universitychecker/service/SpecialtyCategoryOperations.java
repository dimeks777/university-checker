package com.alevel.java.nix.universitychecker.service;

import com.alevel.java.nix.universitychecker.entity.SpecialtyCategory;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyCategoryRequest;

import java.util.List;

public interface SpecialtyCategoryOperations {

    SpecialtyCategory create(SaveSpecialtyCategoryRequest request);

    void update(String name, SaveSpecialtyCategoryRequest request);

    SpecialtyCategory getById(Integer id);

    SpecialtyCategory getByName(String name);

    void deleteByName(String name);

    List<SpecialtyCategory> getAll();

    void deleteAll();

}
