package com.alevel.java.nix.universitychecker.repository;

import com.alevel.java.nix.universitychecker.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Integer> {

    Location findByCity(String city);
}
