package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.UserRoles;
import com.alevel.java.nix.universitychecker.entity.response.UserResponse;
import com.alevel.java.nix.universitychecker.exception.NotEnoughRightsException;
import com.alevel.java.nix.universitychecker.service.UserOperations;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/moderators")
public class ModeratorController {

    private final UserOperations userOperations;

    public ModeratorController(UserOperations userOperations) {
        this.userOperations = userOperations;
    }

    @GetMapping("/users")
    public List<UserResponse> getAllUsers() {
        return userOperations.getAllByRole(UserRoles.USER).stream().map(UserResponse::fromUser).collect(Collectors.toList());
    }

    @DeleteMapping("/users/remove")
    public void deleteUser(@RequestParam Integer user) {
        var existingUser = userOperations.getById(user);
        if (existingUser.getRoles().contains(userOperations.getRoleByName(UserRoles.MODERATOR)))
            throw new NotEnoughRightsException();
        userOperations.deleteById(user);
    }


}
