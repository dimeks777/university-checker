package com.alevel.java.nix.universitychecker.repository;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SpecialtyRepository extends JpaRepository<Specialty, Integer> {

    boolean existsById(Integer specialtyId);

    Optional<Specialty> findByName(String name);

    List<Specialty> findAllBySpecialtyCategory_Name(String name);
}
