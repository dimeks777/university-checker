package com.alevel.java.nix.universitychecker.exception;

import org.springframework.http.HttpStatus;

public class UniversityNotFoundException extends UniversitiesCheckException{
    public UniversityNotFoundException(Integer id) {
        super(HttpStatus.NOT_FOUND, "User with id " + id + "  not found");
    }

    public UniversityNotFoundException(String username) {
        super(HttpStatus.NOT_FOUND, "User " + username + "  not found");
    }
}
