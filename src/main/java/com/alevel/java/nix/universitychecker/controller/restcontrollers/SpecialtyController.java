package com.alevel.java.nix.universitychecker.controller.restcontrollers;

import com.alevel.java.nix.universitychecker.entity.Specialty;
import com.alevel.java.nix.universitychecker.entity.request.SaveSpecialtyRequest;
import com.alevel.java.nix.universitychecker.entity.response.SpecialtyResponse;
import com.alevel.java.nix.universitychecker.service.SpecialtyOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/specialties")
public class SpecialtyController {

    private final SpecialtyOperations specialtyOperations;

    public SpecialtyController(SpecialtyOperations specialtyOperations) {
        this.specialtyOperations = specialtyOperations;
    }

    @GetMapping
    public List<SpecialtyResponse> getAll() {
        List<Specialty> specialties = specialtyOperations.getAll();
        return specialties.stream().map(SpecialtyResponse::fromSpecialty).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public SpecialtyResponse getById(@PathVariable Integer id) {
        var specialty = specialtyOperations.getById(id);
        return SpecialtyResponse.fromSpecialty(specialty);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<SpecialtyResponse> create(@RequestBody SaveSpecialtyRequest request) {
        var specialty = specialtyOperations.create(request);
        return ResponseEntity
                .created(URI.create("/specialties/" + specialty.getId()))
                .body(SpecialtyResponse.fromSpecialty(specialty));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable Integer id, @RequestBody SaveSpecialtyRequest request) {
        specialtyOperations.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id) {
        specialtyOperations.deleteById(id);
    }

}
