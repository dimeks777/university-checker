const updateRequest = document.getElementById("editButton")
const username = document.getElementById('username').value

const url = '/user/' + username;


function setupListener() {

    updateRequest.addEventListener('click', function () {

        editComment();

    })

}


axios.get(url)

    .then(function () {

        console.info('Get request');
        console.info(url);

        setupListener();

    });


function editComment() {

    let commentId2 = document.getElementById('commentId2').value;

    let points = document.getElementById('points').value;

    let text = document.getElementById('text').value;

    console.log(commentId2);
    console.log(points);
    console.log(text);

    if (commentId2 === null || commentId2 === '' || points === null || points === ''

        || text === null || text === '') {

        alert("Fill all the fields");

        return;

    }

    if (commentId2 !== null && points !== null && text !== null) {

        let comment = {
            text: text,
            points: points,
        };

        axios.put('/comments/' + commentId2, comment)

            .then(function () {

                alert("Edited!");

                location.reload();

            })

            .catch(function (reason) {

                alert(reason);
                console.error(reason);

            });

    }

}