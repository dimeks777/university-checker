const createRequest = document.getElementById("createButton")
const Id = document.getElementById('universityId').value

const url = '/universities-find/' + Id;


function setupListener() {

    createRequest.addEventListener('click', function () {

        postComment();

    })

}


axios.get(url)

    .then(function () {

        console.info('Get request');
        console.info(url);
        setupListener();

    });


function postComment() {

    let userId = document.getElementById('userId').value;

    let universityId = document.getElementById('universityId').value;

    let points = document.getElementById('points').value;

    let text = document.getElementById('text').value;

    console.log(userId);
    console.log(universityId);
    console.log(points);
    console.log(text);

    if (userId === null || userId === '' || universityId === null || universityId === ''

        || points === null || points === '' || text === null || text === '') {

        alert("Fill all the fields");

        return;

    }

    if (userId !== null && universityId !== null && points !== null && text !== null) {

        let comment = {
            text: text,
            points: points,
        };

        axios.post('/comments', comment, {
            params: {
                userId,
                universityId
            }
        })

            .then(function () {

                alert("Created!");

                location.reload();

            })

            .catch(function (reason) {

                alert(reason);
                console.error(reason);

            });

    }

}