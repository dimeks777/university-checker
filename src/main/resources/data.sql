BEGIN TRANSACTION;
INSERT INTO roles (name)
VALUES ('ADMIN'),
       ('MODERATOR'),
       ('USER')
ON CONFLICT DO NOTHING;

INSERT INTO users (password, username)
VALUES ('$2a$10$gNeCK1A27Ehs5shFSIVO9OYM8RVJl5XxrzE0L3JvEqjnubBMx1M9y', 'admin'),
       ('$2a$10$dn4nMQyWNSkBIqhO2GpII.HfRvH7rrNd5hO1HM10cuZQwSk2iwf52', 'moderator'),
       ('$2a$10$UrPP5NI7HtckxlU2b5qOkOqzWfvdJCCYQCrLIssmsxDPLOuDHO7B.', 'user')
ON CONFLICT DO NOTHING;

INSERT INTO roles_of_users(user_id, role_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (2, 2),
       (2, 3),
       (3, 3)
ON CONFLICT DO NOTHING;

INSERT INTO specialty_categories(name)
VALUES ('information_technologies'),
       ('natural_sciences'),
       ('humanities'),
       ('management_and_administration'),
       ('law')
ON CONFLICT DO NOTHING;

INSERT INTO specialties(id, name, specialty_category_id)
VALUES (123, 'Computer engineering', 1),
       (122, 'Computer sciences', 1),
       (121, 'Software engineering', 1),
       (101, 'Ecology', 2),
       (102, 'Chemistry', 2),
       (32, 'History and archeology', 3),
       (33, 'Philosophy', 3),
       (72, 'Finances, banking and insurance', 4),
       (75, 'Marketing', 4),
       (81, 'Law', 5),
       (82, 'International law', 5)
ON CONFLICT DO NOTHING;

INSERT INTO locations(city, country)
VALUES ('Kiev', 'Ukraine'),
       ('Kharkiv', 'Ukraine'),
       ('Lviv', 'Ukraine'),
       ('Dnipro', 'Ukraine')
ON CONFLICT DO NOTHING;

INSERT INTO universities(name, official_points, user_points, location_id)
VALUES ('University of Economics and Law "KROK"', 56, NULL, 1),
       ('V.N. Karazin National University ', 80, NULL, 2),
       ('Kharkiv Polytechnic Institute', 70, NULL, 2),
       ('Lviv Polytechnic Institute', 65, NULL, 3),
       ('Alfred Nobel University', 60, NULL, 4)
ON CONFLICT DO NOTHING;

INSERT INTO specialties_of_universities(university_id, specialty_id)
VALUES (1, 72),
       (1, 75),
       (1, 81),
       (1, 82),
       (2, 122),
       (2, 101),
       (2, 32),
       (2, 33),
       (2, 72),
       (2, 75),
       (3, 121),
       (3, 122),
       (3, 123),
       (3, 101),
       (3, 102),
       (3, 72),
       (4, 121),
       (4, 122),
       (4, 123),
       (4, 102),
       (5, 72),
       (5, 75),
       (5, 81)

ON CONFLICT DO NOTHING;

COMMIT;